import PropTypes from 'prop-types';
import React from 'react'
import './index.css'


function Square(props) {
    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
    )
}
Square.propTypes = {
    value: PropTypes.node,
    onClick: PropTypes.func
}

export default Square